import { Component } from 'react'
import { FirebaseAuth } from 'react-firebaseui'

export default class Signin extends Component {
  uiConfig = {
    signInFlow: 'redirect',
    signInOptions: [this.props.firebase.auth.GoogleAuthProvider.PROVIDER_ID],
    callbacks: {
      signInSuccessWithAuthResult (currentUser) {
        console.log('current user test:', currentUser)

        return true
      }
    }
  }

  render () {
    return <FirebaseAuth
      uiConfig={this.uiConfig}
      firebaseAuth={this.props.firebase.auth()}
    />
  }
}
