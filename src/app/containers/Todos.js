import PropTypes from 'prop-types'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import CircularProgress from '@material-ui/core/CircularProgress'
import NoTodos from '../components/NoTodos'
import TodoList from '../components/TodoList'

const enhance = compose(
  firebaseConnect(['/todos#orderByChild=priority']),
  connect(
    ({ firebase: { ordered: { todos } } }) => ({ todos })
  )
)

const Todos = ({ todos }) => {
  if (!isLoaded(todos)) {
    return <CircularProgress color='secondary' />
  }

  if (isEmpty(todos)) {
    return <NoTodos />
  }

  return <TodoList todos={todos} />
}

Todos.propTypes = {
  todos: PropTypes.array
}

export default enhance(Todos)
