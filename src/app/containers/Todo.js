import { Component } from 'react'
import PropTypes from 'prop-types'
import { withFirebase } from 'react-redux-firebase'
import moment from 'moment'

import TodoItem from '../components/TodoItem'

class Todo extends Component {
  static propTypes = {
    firebase: PropTypes
      .shape({ remove: PropTypes.func.isRequired })
      .isRequired,
    todo: PropTypes
      .shape({
        text: PropTypes.string.isRequired,
        completed: PropTypes.bool.isRequired,
        priority: PropTypes.number.isRequired
      })
      .isRequired,
    id: PropTypes.string.isRequired
  }

  state = { remaining: Infinity }

  removeTodo = () => this.props.firebase.remove(`todos/${this.props.id}`)

  raisePriority = () => {
    this.props.firebase.update(
      `todos/${this.props.id}`,
      { priority: this.props.todo.priority + 1 }
    )
  }

  lowerPriority = () => this.props.firebase.update(
    `todos/${this.props.id}`,
    { priority: this.props.todo.priority - 1 }
  )

  setDue = event => this.props.firebase.update(
    `todos/${this.props.id}`,
    { due: moment(event.target.value).valueOf() }
  )

  componentDidMount () {
    this.intervalID = setInterval(
      () => this.tick(),
      1000
    )
  }

  componentWillUnmount () {
    clearInterval(this.intervalID)
  }

  tick () {
    if (this.props.todo.due) {
      const due = moment.utc(this.props.todo.due).valueOf()
      const now = new Date().getTime()
      const remaining = due - now

      this.setState({ remaining })
    }
  }

  render () {
    return <TodoItem
      todo={this.props.todo}
      now={this.props.now}
      removeTodo={this.removeTodo}
      raisePriority={this.raisePriority}
      lowerPriority={this.lowerPriority}
      setDue={this.setDue}
      remaining={this.state.remaining}
    />
  }
}

export default withFirebase(Todo)
