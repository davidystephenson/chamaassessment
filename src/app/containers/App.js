import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { isLoaded, isEmpty, withFirebase } from 'react-redux-firebase'
import moment from 'moment'
import CircularProgress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid'

import Signin from '../containers/Signin'
import Todos from '../containers/Todos'
import Input from '../containers/Input'
import LogoutButton from '../components/LogoutButton'

class App extends Component {
  static propTypes = {
    auth: PropTypes.object,
    firebase: PropTypes
      .shape({
        logout: PropTypes.func.isRequired,
        push: PropTypes.func.isRequired
      })
      .isRequired
  }

  logoutUser = () => this.props.firebase.logout()

  pushTodo = text => this.props.firebase.push(
    'todos',
    {
      text,
      completed: false,
      priority: 0,
      due: moment()
        .add({ hours: 1, minutes: 1 })
        .valueOf()
    }
  )

  render () {
    if (!isLoaded(this.props.auth)) {
      return <CircularProgress />
    }

    if (isEmpty(this.props.auth)) {
      return <Signin firebase={this.props.firebase} />
    }

    return <Grid container spacing={24}>
      <Grid item xs={12}>
        <LogoutButton logoutUser={this.logoutUser} />
      </Grid>

      <Grid item xs={12}>
        <Input pushTodo={this.pushTodo} />
      </Grid>

      <Grid item xs={12}>
        <Todos />
      </Grid>
    </Grid>
  }
}

export default withFirebase(
  connect(
    ({ firebase: { auth } }) => ({ auth })
  )(App)
)
