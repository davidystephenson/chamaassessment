import { Component } from 'react'
import TodoInput from '../components/TodoInput'

export default class Input extends Component {
  state = { value: '' }

  updateValue = event => {
    this.setState({ value: event.target.value })
  }

  pushValue = () => {
    this.props.pushTodo(this.state.value)
    this.setState({ value: '' })
  }

  render () {
    return <TodoInput
      value={this.state.value}
      updateValue={this.updateValue}
      pushValue={this.pushValue}
    />
  }
}
