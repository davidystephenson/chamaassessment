/* global window */

import * as React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import { ReactReduxFirebaseProvider } from 'react-redux-firebase'
import { withRouter } from 'next/router'

import reducer from '../reducers'
import App from '../containers/App'

const config = {
  apiKey: 'AIzaSyAk3DV2-c8bbbtFQgkokqDLJmDt4aqfEwg',
  authDomain: 'chamaassessment.firebaseapp.com',
  databaseURL: 'https://chamaassessment.firebaseio.com',
  projectId: 'chamaassessment',
  storageBucket: 'chamaassessment.appspot.com',
  messagingSenderId: '850756465461'
}

if (!firebase.apps || !firebase.apps.length) {
  firebase.initializeApp(config)
}

const initialState = { firebase: {} }
const devTools = (process.browser && window.__REDUX_DEVTOOLS_EXTENSION__) // SSR for redux dev-tools
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : x => x
const store = createStore(
  reducer,
  initialState,
  devTools
)

const rrfConfig = {
  presence: 'presence',
  sessions: 'sessions',
  userProfile: 'users'
}
const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch
}

const Page = () => (
  <Provider store={store}>
    <ReactReduxFirebaseProvider {...rrfProps}>
      <App />
    </ReactReduxFirebaseProvider>
  </Provider>
)

export default withRouter(Page)
