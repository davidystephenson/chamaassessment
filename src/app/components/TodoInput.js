import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import Grid from '@material-ui/core/Grid'

const styles = theme => ({
  root: {
    alignItems: 'center'
  }
})

const TodoInput = ({ value, updateValue, pushValue, classes }) => {
  const button = value && value !== ''
    ? <Fab color='primary' aria-label='Add' onClick={pushValue}>
      <AddIcon />
    </Fab>
    : null

  return <Grid container spacing={24} className={classes.root}>
    <Grid item>
      <TextField
        id='todo-input'
        label='Add a todo'
        value={value}
        onChange={updateValue}
        margin='normal'
      />
    </Grid>
    <Grid item>
      {button}
    </Grid>
  </Grid>
}

TodoInput.propTypes = {
  value: PropTypes.string.isRequired,
  updateValue: PropTypes.func.isRequired,
  pushValue: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(TodoInput)
