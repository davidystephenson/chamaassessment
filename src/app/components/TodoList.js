import PropTypes from 'prop-types'

import Todo from '../containers/Todo'

const TodoList = ({ todos }) => {
  const items = todos
    .reverse()
    .map(todo => <Todo key={todo.key} id={todo.key} todo={todo.value} />)

  return <div>{items}</div>
}

TodoList.propTypes = {
  todos: PropTypes.array
}

export default TodoList
