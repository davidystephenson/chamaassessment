export default ({ source }) => (
  <audio
    src={source}
    autoPlay
  />
)
