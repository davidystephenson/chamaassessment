import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'

const LogoutButton = ({ logoutUser }) => <Button variant='contained' onClick={logoutUser}>
  {'Logout'}
</Button>

LogoutButton.propTypes = { logoutUser: PropTypes.func.isRequired }

export default LogoutButton
