import Typography from '@material-ui/core/Typography'
import UpIcon from '@material-ui/icons/ArrowUpward'

export default () => (
  <Typography>
    <UpIcon />
    {'Add a todo!'}
    <UpIcon />
  </Typography>
)
