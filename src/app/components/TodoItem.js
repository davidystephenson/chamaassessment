import PropTypes from 'prop-types'
import moment from 'moment'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import ErrorIcon from '@material-ui/icons/Error'
import WarningIcon from '@material-ui/icons/Warning'

import AudioPlayer from './AudioPlayer'

const styles = theme => ({
  card: {
    marginBottom: theme.spacing.unit * 2
  },
  dueSoon: {
    color: 'goldenrod'
  },
  overdue: {
    color: 'red'
  },
  flex: {
    display: 'flex'
  },
  icon: {
    marginRight: theme.spacing.unit
  }
})

const TodoItem = ({
  todo,
  removeTodo,
  raisePriority,
  lowerPriority,
  setDue,
  remaining,
  classes
}) => {
  const lowerButton = todo.priority > 0
    ? <Button size='small' color='secondary' onClick={lowerPriority}>Lower Priority</Button>
    : null

  const soon = 45 * 60 * 1000
  const dueSoon = remaining < soon
  const overdue = remaining < 0

  const checkSchedule = (overdueContent, dueSoonContent, defaultContent = null) => overdue
    ? overdueContent
    : dueSoon
      ? dueSoonContent
      : defaultContent
  const audio = checkSchedule(
    <AudioPlayer source={'https://freesound.org/data/previews/371/371177_5487341-lq.mp3'} />,
    <AudioPlayer source={'https://freesound.org/data/previews/72/72128_1028972-lq.mp3'} />
  )
  const icon = checkSchedule(
    <ErrorIcon className={classes.icon} />,
    <WarningIcon className={classes.icon} />
  )
  const scheduleClass = checkSchedule('overdue', 'dueSoon')

  const displayDate = todo.due
    ? moment(todo.due).format('YYYY-MM-DDTHH:mm')
    : null
  const textField = displayDate
    ? <TextField
      id='datetime-local'
      label='Change Due Date'
      type='datetime-local'
      defaultValue={displayDate}
      InputLabelProps={{
        shrink: true
      }}
      onChange={setDue}
    />
    : null

  const instance = moment(todo.due)
  const calendar = instance.calendar()
  const fromNow = instance.fromNow()

  return <Card className={classes.card}>
    <CardHeader title={todo.text} />
    <CardContent>
      <Typography variant='h5' className={`${classes[scheduleClass]} ${classes.flex}`}>
        {icon}
        Due {fromNow}
      </Typography>

      <Typography>
        {calendar}
      </Typography>
    </CardContent>
    <Divider />
    <CardContent>
      {textField}
    </CardContent>
    <Divider />
    <CardContent>
      <Typography variant='h5'>
        Priority {todo.priority}
      </Typography>
    </CardContent>
    <CardActions>
      <Button size='small' color='primary' onClick={raisePriority}>
        Raise Priority
      </Button>

      {lowerButton}

      <Button size='small' color='secondary' onClick={removeTodo}>Delete</Button>
      {audio}
    </CardActions>
  </Card>
}

TodoItem.propTypes = {
  todo: PropTypes.shape({
    text: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
    priority: PropTypes.number.isRequired
  }).isRequired,
  raisePriority: PropTypes.func.isRequired,
  lowerPriority: PropTypes.func.isRequired,
  removeTodo: PropTypes.func.isRequired,
  setDue: PropTypes.func.isRequired,
  remaining: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(TodoItem)
