const withCSS = require('@zeit/next-css') // Needed to load the firebaseui CSS

module.exports = withCSS({ distDir: '../../dist/functions/next' })
